from . import views
from django.urls import path, include

urlpatterns = [
    path('', views.accueil, name='accueil'),
    path('<slug>', views.lire_article, name='blog_lire'),
]
